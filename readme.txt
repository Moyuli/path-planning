A* for 2D grid

By Moyu Li

The first program is AStarSearch.java.
This program gives out an optimal path from start to target.

To run this file in Mac OS:
1. open terminal
2. go to the file directory
3. compile with command: javac AStarSearch.java
4. run with command: java AStarSearch test1.txt or java AStarSearch test2.txt

The program will print out the cost and an optimal path.

The test 1 and test 2 are input maps.


The second program is AStarSearch_hammer.java.
This program gives out an optimal path from start to target, with the opportunity to use the hammer once.

To run this file in Mac OS:
1. open terminal
2. go to the file directory
3. compile with command: javac AStarSearch_hammer.java
4. run with command: java AStarSearch_hammer test3.txt or java AStarSearch_hammer test4.txt
    or  java AStarSearch_hammer test5.txt  or   java AStarSearch_hammer test6.txt

This program will print out an optimal path, with the opportunity to use the hammer once.
When it has to use hammer, it will print out “has to use hammer”.

The test 3 and test 4 is the same as given in the hackerrank.
The test5 tests another case when the robot has to use the hammer to get to its target and not to the hammer to optimize path.
The test6 tests the case when there is no solution even with using the hammer.

All test results has been confirmed correct. 

Assumptions:
The cost g is calculated by the distance moved from start to the point.
The cost h is calculated by the distance from the point to the target.
The final cost (f) is calculated by adding the g and the h.
